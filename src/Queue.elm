
module Queue exposing (..)

import List

-- TODO this is a placeholder module

type alias Queue a = List a

empty : Queue a
empty = []

pushBack : a -> Queue a -> Queue a
pushBack x xs = xs ++ [x]

popFront : Queue a -> Maybe (a, Queue a)
popFront xs = case xs of
  [] -> Nothing
  x :: xs -> Just (x, xs)

