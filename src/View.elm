
module View exposing (view)

import Html exposing (Html, button, div, text)
import Html.Events exposing (onClick)

import Svg exposing (Svg, Attribute)
import Svg.Attributes as Attributes exposing (x,y,width,height,fill,fontFamily,textAnchor, style)
import Svg.Events exposing (onMouseOver, onMouseOut, onMouseDown, onMouseUp)

import Matrix exposing (loc, col, row, flatten)

import Model exposing (..)
import Update exposing (..)

import Set

view : Model -> Html Msg
view model =
  let
    windowSize = (1000, 1000)
    tileSize = 48
    attributes = [ x "100"
                 , y "100"
                 , textAnchor "middle"
                 , fontFamily normalFontFamily
                 , Attributes.fontSize "20"
                 , fill black
                 ]
    rectify : Model -> Tile -> Svg Msg
    rectify model tile =
      let
        colour = if Set.member tile.gridPosition model.highlighted then "rgb(0,0,255)" else "rgb(255,0,0)"
        l = tile.gridPosition
        (px, py) = tile.position
      in
        Svg.rect [
          x (toString <| py),
          y (toString <| px),
          width (toString <| tileSize),
          height (toString <| tileSize),
          style ("fill:" ++ colour ++ ";stroke-width:1;stroke:rgb(0,0,0);" ++ "fill-opacity:" ++ (toString tile.opacity)),
          onMouseOver (Selection l),
          onMouseOut (Deselection l),
          onMouseDown (SelectionStart l),
          onMouseUp (SelectionEnd l)
        ] []
    grid = Matrix.map (rectify model) model.grid |> flatten
  in
  div [] [
    div [] [
      --text (toString model)
      text (toString model.selected),
      text (toString model.selection)
    ],
    Svg.svg (svgAttributes windowSize) ([
        Svg.text' (attributes ++ [fill black]) [
          --Svg.text "hi"
        ]
    ] ++ grid)
  ]

normalFontFamily : String
normalFontFamily =
  "Courier New, Courier, Monaco, monospace"

black : String
black = "rgba(0,0,0,1)"

svgAttributes : (Int, Int) -> List (Attribute Msg)
svgAttributes (w, h) =
  [ width (toString w)
  , height (toString h)
  , Attributes.viewBox <| "0 0 " ++ (toString w) ++ " " ++ (toString h)
  , Attributes.style "position: fixed;"
  ]

