
module Update exposing (..)

import Model exposing (..)
--import Matrix exposing (Location, matrix, col, row)
import Matrix exposing (..)

import PriQueue exposing (..)
import Queue exposing (..)

import AnimationFrame
import List
import Set

type Msg
  = Selection Location
  | Deselection Location
  | SelectionStart Location
  | SelectionEnd Location
  | SelectionDone (List Location)
  | Tick Float

unwrap : Maybe a -> a
unwrap v = case v of
  Just x -> x
  Nothing -> Debug.crash "failed"

update : Msg -> Model -> (Model, Cmd Msg)
update msg model =
  case msg of
    Selection l ->
      if model.selected
        then (select l model, Cmd.none)
        else (highlight l model, Cmd.none)
    Deselection l ->
      if model.selected
        then (model, Cmd.none)
        --else ({ model | grid = (Matrix.update l (\x -> { x | selected = False }) model.grid) }, Cmd.none)
        else ({ model | highlighted = Set.remove l model.highlighted }, Cmd.none)
    SelectionStart l ->
      -- TODO this can happen twice if selection leaves the svg element and re-enters somewhere else.
      -- Have some kind of integrity check for that?
      (model |> select l |> startSelection, Cmd.none)
    SelectionEnd l ->
      let
        finalSelection = model.selection
        -- TODO fix the partial access
        next = SelectionDone (finalSelection |> List.reverse)
        model' = { model | selection = finalSelection }
      in
        if model'.selected
          then update next (clearSelection model')
          else (model', Cmd.none)
    SelectionDone ls ->
      let _ = Debug.log "selected" ls in
      let res = destroyTiles ls model in
      (res, Cmd.none)
    Tick dt ->
      ({ model | grid = Matrix.map (stepTile dt) model.grid }, Cmd.none)

dimensions : Matrix a -> (Int, Int)
dimensions matrix = (rowCount matrix, colCount matrix)

-- TODO does not consider diagonals yet
immediateNeighbours : Model -> Location -> List Location
immediateNeighbours model l =
  let
    (r', c') = dimensions model.grid
    (r, c) = (row l, col l)
    candidates = [(r + 1, c), (r - 1, c), (r, c + 1), (r, c - 1)]
  in
    candidates |> List.filter (\(r, c) -> r >= 0 && c >= 0 && r < r' && c < c')

onEdge : Model -> Location -> Bool
onEdge model l =
  let
    (r', c') = dimensions model.grid
    (r, c) = (row l, col l)
  in
    r == 0 || r == (r' - 1) || c == 0 || c == (c' - 1)

isEmpty : List Location -> Location -> Bool
isEmpty = flip List.member

withAllNeighbours : Model -> Location -> Queue Location -> Queue Location
withAllNeighbours model l empty =
  let _ = Debug.log "empty" (empty) in
  immediateNeighbours model l
    |> List.foldr Queue.pushBack empty
    |> List.filter (\x -> not <| List.member x empty)

closestNeighbour : Model -> PriQueue Location -> Location -> Maybe Location
closestNeighbour model (empty, _) l =
  let
    _ = Debug.log "finding closest neighbour. these are empty" empty
    grid = model.grid
    queue = withAllNeighbours model l empty
    aux q visited limit =
      let
        _ = if limit > 20 then Debug.crash "dead" else 1
        --_ = Debug.log "new contents of q and visited set" (q, visited)
        hole = Queue.popFront q
      in
        case hole of
          Nothing ->
            Nothing
            --Debug.crash <| "cannot find closest neighbour of " ++ toString hole
          Just (hole', q') ->
            if isEmpty empty hole'
              then aux (withAllNeighbours model hole' q') (Set.insert hole' visited) (limit + 1)
              else Just hole'
  in aux queue Set.empty 0

distance : Location -> Location -> Float
distance (ax, ay) (bx, by) = sqrt ((toFloat ax - toFloat bx) ^ 2 + (toFloat ay - toFloat by) ^ 2)

destroyTiles : List Location -> Model -> Model
destroyTiles ls model =
  let
    centre = dimensions model.grid
    priQueue = PriQueue.ofList ls (\al bl -> compare (distance al centre) (distance bl centre))
    locked = Set.empty
    go q model locked =
      if PriQueue.isEmpty q
        then
          let _ = Debug.log "queue is empty. model grid" (q, model.grid |> flatten |> List.filter (\x -> x.position /= (fromLocation x.gridPosition))) in
          model
        else
          let
            (current, q') = case PriQueue.removeMin q of
              Nothing -> Debug.crash "nothing left in the queue, now what"
              Just res -> res
            --_ = Debug.log "popped" current
            (emptyTiles, _) = q'
            (model', newlyEmpty) = destroyTile current model (emptyTiles ++ Set.toList locked)
            q'' = List.foldr PriQueue.add q' newlyEmpty
            locked' = Set.insert current locked
            --_ = Debug.log "queue is not empty. model grid" (q'', model'.grid |> flatten |> List.filter (\x -> x.position /= (fromLocation x.location)))
          in
            go q'' model' locked'
  in
    go priQueue model locked

-- | Takes the position of the tile to destroy, tiles which are invalid as neighbours, and the model.
-- | Returns an updated model (with tiles shifted around) and additions to the set of empty tiles.
destroyTile : Location -> Model -> List Location -> (Model, List Location) -- TODO use set
destroyTile l model empty =
  case closestNeighbour model (empty, \x y -> EQ) l of
    Nothing ->
      (spawnTile l model, [])
    Just neighbour ->
      let
        neighbourTile = (unwrap <| Matrix.get neighbour model.grid) |> (\t -> { t | gridPosition = l })
        model' = { model | grid = Matrix.set l neighbourTile model.grid }
        --_ = Debug.log "replacing with" (neighbour)
      in
        if onEdge model' neighbour
          then
            (spawnTile neighbour model', [])
          else (model', [neighbour])

spawnTile : Location -> Model -> Model
spawnTile l model =
  let
    tile = defaultTile l
    tile' = { tile | opacity = 0 }
  in
    { model | grid = Matrix.set l tile' model.grid }

select : Location -> Model -> Model
select l model =
  { model | selection = l :: model.selection } |> highlight l

startSelection : Model -> Model
startSelection model =
  -- TODO unhiglight all with a fold
  { model | selected = True }

clearSelection : Model -> Model
clearSelection model =
  let
    selection = model.selection
    model' = { model | selection = [], selected = False }
  in
    List.foldr unhighlight model' selection

highlight : Location -> Model -> Model
highlight l model =
  { model | highlighted = Set.insert l model.highlighted }

unhighlight : Location -> Model -> Model
unhighlight l model =
  { model | highlighted = Set.remove l model.highlighted }

subscriptions : Model -> Sub Msg
subscriptions _ = Sub.batch [AnimationFrame.diffs Tick]

init : (Model, Cmd Msg)
init =
  let
    grid = matrix 10 10 (\l -> { x = col l, y = row l, selected = False })
  in
    (initialModel, Cmd.none)
