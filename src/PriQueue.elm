
module PriQueue exposing (..)

import List
import Queue exposing (popFront)

-- TODO this is a placeholder module

type alias PriQueue a = (List a, a -> a -> Order)

ofList : List a -> (a -> a -> Order) -> PriQueue a
ofList = (,)

empty : PriQueue a
empty = ([], \_ _ -> EQ)

isEmpty : PriQueue a -> Bool
isEmpty (xs, _) = case xs of
  [] -> True
  _ -> False

add : a -> PriQueue a -> PriQueue a
add x (xs, cmp) = (x :: xs, cmp)

removeMin : PriQueue a -> Maybe (a, PriQueue a)
removeMin (xs, cmp) =
  let xs' = List.sortWith cmp xs in
  Maybe.map (\(a, b) -> (a, (b, cmp))) <| popFront xs'

