
module Model exposing (..)

import List

import Set exposing (Set)

import Matrix exposing (Matrix, Location, matrix, col, row)

type alias Position = (Float, Float)

type alias Tile = {
  gridPosition : Location,
  position : Position,
  opacity : Float
}

defaultTile : Location -> Tile
defaultTile l = {
    gridPosition = l,
    position = fromLocation l,
    opacity = 1
  }

posDistance : Position -> Position -> Float
posDistance (ax, ay) (bx, by) = sqrt ((ax - bx) ^ 2 + (ay - by) ^ 2)

atPosition : Tile -> Bool
atPosition tile = posDistance tile.position (fromLocation tile.gridPosition) < 0.000001

stepTile : Float -> Tile -> Tile
stepTile dt = updatePosition dt >> updateOpacity dt

updateOpacity : Float -> Tile -> Tile
updateOpacity dt t =
  let threshold = 0.01 in
  if abs (t.opacity - 1) < threshold
    then { t | opacity = 1 }
    else { t | opacity = t.opacity + dt * 0.001 }

-- TODO use better syntax
updatePosition : Float -> Tile -> Tile
updatePosition dt t =
  let
    (ax, ay) = t.position
    (bx, by) = fromLocation t.gridPosition
    (vx, vy) = (bx - ax, by - ay)
    magnitude = posDistance t.position (fromLocation t.gridPosition)
    speed = 0.3
  in
    if magnitude < speed
      then t
      else
        let
          destination = fromLocation t.gridPosition
          (ux, uy) = (vx / magnitude, vy / magnitude)
          v' = (ux * dt * speed, uy * dt * speed)
          (vx', vy') = v'
          (tx, ty) = t.position
          (dx, dy) = (tx + vx', ty + vy')
        in
          if posDistance destination (dx, dy) < 0.04
            then { t | position = destination }
            else { t | position = (dx, dy) }

type alias Model = {
  grid : Matrix Tile,
  selected : Bool,
  selection : List Location, -- TODO this should be a list but with distinct elements
  highlighted : Set Location
}

fromLocation : Location -> Position
fromLocation l = (toFloat <| col l * 50, toFloat <| row l * 50)

initialModel : Model
initialModel = {
    grid = matrix 3 3 defaultTile,
    selected = False,
    selection = [],
    highlighted = Set.empty
  }
