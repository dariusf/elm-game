
module Main exposing (main)

import View exposing (view)
import Update exposing (init, update, subscriptions)

import Html.App as App

main : Program Never
main =
  App.program {
    init = init,
    update = update,
    view = view,
    subscriptions = subscriptions
  }
